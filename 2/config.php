<?php
// HTTP
define('HTTP_SERVER', 'http:///');

// HTTPS
define('HTTPS_SERVER', 'http:///');

// DIR
$path = '';//SAE_TMP_PATH ;
define('DIR_APPLICATION',  $path . 'catalog/');
define('DIR_SYSTEM', $path . 'system/');
define('DIR_DATABASE', $path . 'system/database/');
define('DIR_LANGUAGE', $path . 'catalog/language/');
define('DIR_TEMPLATE', $path . 'catalog/view/theme/');
define('DIR_CONFIG', $path . 'system/config/');
define('DIR_IMAGE', $path . 'image/');
define('DIR_CACHE', $path . 'system/cache/');
define('DIR_DOWNLOAD', $path . 'download/');
define('DIR_LOGS', $path . 'system/logs/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', SAE_MYSQL_HOST_M . ':' . SAE_MYSQL_PORT);
define('DB_USERNAME', SAE_MYSQL_USER);
define('DB_PASSWORD', SAE_MYSQL_PASS);
define('DB_DATABASE', SAE_MYSQL_DB);
define('DB_PREFIX', 'fm_');
?>
