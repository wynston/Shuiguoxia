<?php
// HTTP
define('HTTP_SERVER', '');
define('HTTP_CATALOG', '');

// HTTPS

define('HTTPS_SERVER', '');
define('HTTPS_CATALOG', '');
// DIR
define("ROOT", $_SERVER['DOCUMENT_ROOT']);
$path = ROOT .'/';



define('DIR_APPLICATION',$path .  'admin/');
define('DIR_SYSTEM',$path .  'system/');
define('DIR_DATABASE', $path . 'system/database/');
define('DIR_LANGUAGE',$path .  'admin/language/');
define('DIR_TEMPLATE',$path .  'admin/view/template/');
define('DIR_CONFIG',$path .  'system/config/');
define('DIR_IMAGE',$path .  'image/');
define('DIR_CACHE',$path .  'system/cache/');
define('DIR_DOWNLOAD',$path .  'download/');
define('DIR_LOGS',$path .  'system/logs/');
define('DIR_CATALOG', $path . 'catalog/');




// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', SAE_MYSQL_HOST_M . ':' . SAE_MYSQL_PORT);
define('DB_USERNAME', SAE_MYSQL_USER);
define('DB_PASSWORD', SAE_MYSQL_PASS);
define('DB_DATABASE', SAE_MYSQL_DB);
define('DB_PREFIX', 'fm_');
?>
